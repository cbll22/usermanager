// hier wird die "User" class festgelegt mit den Attributen Vorname, Nachname, E-mail und Passwort
class User {
    vname: string;
    nname: string;
    email: string;
    pw: string;
    constructor(vname: string, nname: string, email: string, pw: string) {
        this.vname = vname;
        this.nname = nname;
        this.email = email;
        this.pw = pw;
    }
}
//hier wird der Array "users" erstellt
const users: User[] = [];
//dieses Event bestimmt, dass der DOM erst vollständig geladen werden muss bevor was passiert
document.addEventListener("DOMContentLoaded", () => {
    //hier werden die Variabeln den Formfield Inputs zugeordnet
    const vnameInput: HTMLInputElement = document.querySelector("#vnameInput");
    const nnameInput: HTMLInputElement = document.querySelector("#nnameInput");
    const emailInput: HTMLInputElement = document.querySelector("#emailInput");
    const pwInput: HTMLInputElement = document.querySelector("#pwInput");
    const formInput: HTMLFormElement = document.querySelector("#formInput");
    const formEdit: HTMLFormElement = document.querySelector("#formEdit");
    const tableNutzer = document.getElementById("tableNutzer");
    const vnameEdit: HTMLInputElement = document.querySelector("#vnameEdit");
    const nnameEdit: HTMLInputElement = document.querySelector("#nnameEdit");
    const emailEdit: HTMLInputElement = document.querySelector("#emailEdit");

//dieser Eventlistener triggert die "addUser" Funktion und den Checker, um zu prüfen ob der "users" Array leer ist
    formInput.addEventListener("submit", (event)=>{
        addUser(event);
        checker();
    });
    //dieser Eventlistener triggert die "editUser" Funktion
    formEdit.addEventListener("submit", editUser);
    //das sind die  Eventlistener für die Lösch und Edit Funktion, bei der Edit Funktion wird ebenfalls der Checker getriggert um die Tablle auszublenden, wenn der Array leer ist
    tableNutzer.addEventListener("click", (event: Event) => {

        if (event.target instanceof HTMLElement && event.target.matches(".deletebtn")) {
            deleteUser(event);
            checker();
        }
        if (event.target instanceof HTMLElement && event.target.matches(".editbtn")) {
            showEdit(event);
        }

    })

//die Funktion erzeugt die Tabelle
    function renderTable(): void {

        while (tableNutzer.hasChildNodes()) {
            tableNutzer.removeChild(tableNutzer.firstChild);
        }
//diese Schleife wiederholt sich für jedes Objekt, dass sich im Array findet. Immer wenn es einen weiteren User findet, wird eine neue Tabellenzeile erzeugt
        for (let i = 0; i < users.length; i++) {
            let user = users[i];
//hier wird der entsprechende HTML Code für jede Tabellenzeile/User erzeugt, sowie die Lösch und Edit Buttons
            tableNutzer.innerHTML +=
                `
                <tr data-index="${i}">
                    <td> ${user.vname} </td>
                    <td> ${user.nname} </td>
                    <td> ${user.email} </td>
                    <td>
                        <button class="btn editbtn" id="schreibButton">
                             <a href="#">
                                <img class="editbtn" alt="pencil" height="25" src="icons/pencil.png">
                             </a>
                         </button>
                        <button class="btn deletebtn" id="loeschButton">
                            <a href="#">
                                <img class ="deletebtn" alt="trashcan" height="20" src="icons/trashcan.png">
                             </a>
                        </button>
                    </td>
                </tr>
                `

        }
    }

//die Löschfunktion sucht sich den entsprechenden Eintrag aus dem Array (abhängig davon welcher Löschbutton geklickt wurde) und entfernt ihn mit der Splice Methode, anschließend generiert er die Tabelle neu
    function deleteUser(event: Event): void {
        let find = (event.target as HTMLElement).closest("tr");
        let index = Number(find.dataset.index);
        users.splice(index, 1);
        renderTable();
    }

//Funktion um einen User hinzuzufügen A. Fragen wegen Event
    function addUser(event: Event) {
        //das hier verhindert, dass die Seite beim ausführen der Funktion neu geladen wird
        event.preventDefault();
//legt die Inputs aus den Feldern als Werte für das Objekt fest
        const vname: string = vnameInput.value;
        const nname: string = nnameInput.value;
        const email: string = emailInput.value;
        const pw: string = pwInput.value;
//prüft ob in alle Felder etwas eingegeben wurde
        if (vname.length === 0 || nname.length === 0 || email.length === 0 || pw.length === 0) {
            formInput.reportValidity();
            //wenn ja fügt es die Werte dem Objekt zu
        } else {
            for (let user of users) {
                if (email === user.email) return;
            }
            users.push(new User(vname, nname, email, pw));
            //löscht die Einträge aus den Submitfeldern und läd die Tabelle neu
            formInput.reset();

            renderTable();
        }
    }

//Funktion um das Editfenster anzuzeigen, dass standardmässig versteckt ist
    function showEdit(event: Event): void {
//hier sucht sich die Funktion den entsprechenden Eintrag (wieder vom Button abhängig)
        let find = (event.target as HTMLElement).closest("tr");
        let i = Number(find.dataset.index);
//hier holen wir uns die Werte der jeweiligen Variablen, die daraufhin im Editfenster angezeigt werden.
        emailEdit.value = users[i].email;
        vnameEdit.value = users[i].vname;
        nnameEdit.value = users[i].nname;
   //das HTML Element "editSec" wird gesucht und das Attribut "display" wird auf block gesetzt, damit es auf der Seite angezeigt wird
        const sec = document.getElementById("editSec");
        sec.style.display = 'block';
    }

//die Funktion erlaubt es die Werte der Variablen "vname" und "nname" zu ändern
    function editUser(event: Event) {
        event.preventDefault();
//sucht sich den Wert der Variabel "email" des entsprechenden Objekts um es in dem nicht editierbare Formfield anzuzeigen
        let i = users.map(x => {
            return x.email
        }).indexOf(emailEdit.value);
//hier wird festgelegt, dass die Werte der Variablen vom Input aus den Edit-Feldern überschrieben wird
        const vned: string = vnameEdit.value;
        const nned: string = nnameEdit.value;
//prüft ob die Felder leer sind
        if (vned.length === 0 || nned.length === 0) {
            formEdit.reportValidity();
        }
        //wenn nicht, dann werden die entsprechenden Werte aus den Edit-Feldern übernommen
        else {
            users[i].vname = vned;
            users[i].nname = nned;
//Hier wird die Tablle neu erzeugt und die "stopEdit" Funktion getriggert, die dass Fenster wieder schließt
            renderTable();
            stopEdit();
        }
        stopEdit();
//das ist die abbrechen Funktion die das Fenster wieder schließen sollte, ohne dass Änderungen vorgenommen werden
// funktioniert!!
    }
    let cancel = document.getElementById("cancelEdit");
    cancel.addEventListener('click', function handleClick() {
        stopEdit();
    });
//die Funktion versteckt das Feld wieder, wenn die Änderung abgeschickt, oder der "Abbrechen" Button geklickt wird
    function stopEdit() {
    const sec = document.getElementById("editSec");
    sec.style.display = "none";
    }
//die Checker Funktion versteckt die Tabelle, wenn sie keinen Eintrag enthält
    function checker(){
        const tabelle =document.getElementById("tabelle")
        //wenn der Array keine Einträge hat, also 0 ist, versteckt die Funktion die Tabelle. Wenn sie einen Eintrag hat, also größer als 0 ist, wird sie angezeigt.
        if (users.length === 0) {
            tabelle.style.display = "none"
        }
        else
        {
            tabelle.style.display = "block"
        }
    }

})

