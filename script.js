// hier wird die "User" class festgelegt mit den Attributen Vorname, Nachname, E-mail und Passwort
var User = /** @class */ (function () {
    function User(vname, nname, email, pw) {
        this.vname = vname;
        this.nname = nname;
        this.email = email;
        this.pw = pw;
    }
    return User;
}());
//hier wird der Array "users" erstellt
var users = [];
//dieses Event bestimmt, dass der DOM erst vollständig geladen werden muss bevor was passiert
document.addEventListener("DOMContentLoaded", function () {
    //hier werden die Variabeln den Formfield Inputs zugeordnet
    var vnameInput = document.querySelector("#vnameInput");
    var nnameInput = document.querySelector("#nnameInput");
    var emailInput = document.querySelector("#emailInput");
    var pwInput = document.querySelector("#pwInput");
    var formInput = document.querySelector("#formInput");
    var formEdit = document.querySelector("#formEdit");
    var tableNutzer = document.getElementById("tableNutzer");
    var vnameEdit = document.querySelector("#vnameEdit");
    var nnameEdit = document.querySelector("#nnameEdit");
    var emailEdit = document.querySelector("#emailEdit");
    //dieser Eventlistener triggert die "addUser" Funktion und den Checker, um zu prüfen ob der "users" Array leer ist
    formInput.addEventListener("submit", function (event) {
        addUser(event);
        checker();
    });
    //dieser Eventlistener triggert die "editUser" Funktion
    formEdit.addEventListener("submit", editUser);
    //das sind die  Eventlistener für die Lösch und Edit Funktion, bei der Edit Funktion wird ebenfalls der Checker getriggert um die Tablle auszublenden, wenn der Array leer ist
    tableNutzer.addEventListener("click", function (event) {
        if (event.target instanceof HTMLElement && event.target.matches(".deletebtn")) {
            deleteUser(event);
            checker();
        }
        if (event.target instanceof HTMLElement && event.target.matches(".editbtn")) {
            showEdit(event);
        }
    });
    //die Funktion erzeugt die Tabelle
    function renderTable() {
        while (tableNutzer.hasChildNodes()) {
            tableNutzer.removeChild(tableNutzer.firstChild);
        }
        //diese Schleife wiederholt sich für jedes Objekt, dass sich im Array findet. Immer wenn es einen weiteren User findet, wird eine neue Tabellenzeile erzeugt
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            //hier wird der entsprechende HTML Code für jede Tabellenzeile/User erzeugt, sowie die Lösch und Edit Buttons
            tableNutzer.innerHTML +=
                "\n                <tr data-index=\"".concat(i, "\">\n                    <td> ").concat(user.vname, " </td>\n                    <td> ").concat(user.nname, " </td>\n                    <td> ").concat(user.email, " </td>\n                    <td>\n                        <button class=\"btn editbtn\" id=\"schreibButton\">\n                             <a href=\"#\">\n                                <img class=\"editbtn\" alt=\"pencil\" height=\"25\" src=\"icons/pencil.png\">\n                             </a>\n                         </button>\n                        <button class=\"btn deletebtn\" id=\"loeschButton\">\n                            <a href=\"#\">\n                                <img class =\"deletebtn\" alt=\"trashcan\" height=\"20\" src=\"icons/trashcan.png\">\n                             </a>\n                        </button>\n                    </td>\n                </tr>\n                ");
        }
    }
    //die Löschfunktion sucht sich den entsprechenden Eintrag aus dem Array (abhängig davon welcher Löschbutton geklickt wurde) und entfernt ihn mit der Splice Methode, anschließend generiert er die Tabelle neu
    function deleteUser(event) {
        var find = event.target.closest("tr");
        var index = Number(find.dataset.index);
        users.splice(index, 1);
        renderTable();
    }
    //Funktion um einen User hinzuzufügen A. Fragen wegen Event
    function addUser(event) {
        //das hier verhindert, dass die Seite beim ausführen der Funktion neu geladen wird
        event.preventDefault();
        //legt die Inputs aus den Feldern als Werte für das Objekt fest
        var vname = vnameInput.value;
        var nname = nnameInput.value;
        var email = emailInput.value;
        var pw = pwInput.value;
        //prüft ob in alle Felder etwas eingegeben wurde
        if (vname.length === 0 || nname.length === 0 || email.length === 0 || pw.length === 0) {
            formInput.reportValidity();
            //wenn ja fügt es die Werte dem Objekt zu
        }
        else {
            for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                var user = users_1[_i];
                if (email === user.email)
                    return;
            }
            users.push(new User(vname, nname, email, pw));
            //löscht die Einträge aus den Submitfeldern und läd die Tabelle neu
            formInput.reset();
            renderTable();
        }
    }
    //Funktion um das Editfenster anzuzeigen, dass standardmässig versteckt ist
    function showEdit(event) {
        //hier sucht sich die Funktion den entsprechenden Eintrag (wieder vom Button abhängig)
        var find = event.target.closest("tr");
        var i = Number(find.dataset.index);
        //hier holen wir uns die Werte der jeweiligen Variablen, die daraufhin im Editfenster angezeigt werden.
        emailEdit.value = users[i].email;
        vnameEdit.value = users[i].vname;
        nnameEdit.value = users[i].nname;
        //das HTML Element "editSec" wird gesucht und das Attribut "display" wird auf block gesetzt, damit es auf der Seite angezeigt wird
        var sec = document.getElementById("editSec");
        sec.style.display = 'block';
    }
    //die Funktion erlaubt es die Werte der Variablen "vname" und "nname" zu ändern
    function editUser(event) {
        event.preventDefault();
        //sucht sich den Wert der Variabel "email" des entsprechenden Objekts um es in dem nicht editierbare Formfield anzuzeigen
        var i = users.map(function (x) {
            return x.email;
        }).indexOf(emailEdit.value);
        //hier wird festgelegt, dass die Werte der Variablen vom Input aus den Edit-Feldern überschrieben wird
        var vned = vnameEdit.value;
        var nned = nnameEdit.value;
        //prüft ob die Felder leer sind
        if (vned.length === 0 || nned.length === 0) {
            formEdit.reportValidity();
        }
        //wenn nicht, dann werden die entsprechenden Werte aus den Edit-Feldern übernommen
        else {
            users[i].vname = vned;
            users[i].nname = nned;
            //Hier wird die Tablle neu erzeugt und die "stopEdit" Funktion getriggert, die dass Fenster wieder schließt
            renderTable();
            stopEdit();
        }
        stopEdit();
        //das ist die abbrechen Funktion die das Fenster wieder schließen sollte, ohne dass Änderungen vorgenommen werden
        // funktioniert!!
    }
    var cancel = document.getElementById("cancelEdit");
    cancel.addEventListener('click', function handleClick() {
        stopEdit();
    });
    //die Funktion versteckt das Feld wieder, wenn die Änderung abgeschickt, oder der "Abbrechen" Button geklickt wird
    function stopEdit() {
        var sec = document.getElementById("editSec");
        sec.style.display = "none";
    }
    //die Checker Funktion versteckt die Tabelle, wenn sie keinen Eintrag enthält
    function checker() {
        var tabelle = document.getElementById("tabelle");
        //wenn der Array keine Einträge hat, also 0 ist, versteckt die Funktion die Tabelle. Wenn sie einen Eintrag hat, also größer als 0 ist, wird sie angezeigt.
        if (users.length === 0) {
            tabelle.style.display = "none";
        }
        else {
            tabelle.style.display = "block";
        }
    }
});
//# sourceMappingURL=script.js.map